#include "Dvd.h"

Dvd::Dvd(): Product(), movie_studio(""), male_lead(""), female_lead("")
{ }

Dvd::Dvd(int new_id, string new_name, double new_price, int new_stock, string new_movie_studio, string new_male_lead, string new_female_lead):
	Product(new_id, new_name, new_price, new_stock)
{
	movie_studio = new_movie_studio;
	male_lead = new_male_lead;
	female_lead = new_female_lead;
}

void Dvd::print_info()
{
	cout << "Dvd" << endl;
	Product::print_info();
	cout << "Movie studio\t: " << movie_studio << endl
		<< "Male lead\t: " << male_lead << endl
		<< "Female lead\t: " << female_lead << endl;
}

istream& operator >> (istream &in, Dvd &my_dvd)
{
	// in >> static_cast<Product>(my_dvd) >> my_dvd.movie_studio >> my_dvd.male_lead >> my_dvd.female_lead;
	in >> my_dvd.pId >> my_dvd.pName >> my_dvd.pPrice >> my_dvd.pStock >> my_dvd.movie_studio >> my_dvd.male_lead >> my_dvd.female_lead;
	return in;
}

ostream& operator << (ostream &out, Dvd &my_dvd)
{
	// out << static_cast<Product>(my_dvd) << endl
	out << my_dvd.pId << endl
		<< my_dvd.pName << endl
		<< my_dvd.pPrice << endl
		<< my_dvd.pStock << endl
		<< my_dvd.male_lead << endl
		<< my_dvd.female_lead;
	return out;
}
