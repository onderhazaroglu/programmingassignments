#include "Product.h"

Product::Product(): pId(0), pName(""), pPrice(0.0), pStock(0)
{ }

Product::Product(int new_id, string new_name, double new_price, int new_stock):
	pId(new_id), pName(new_name), pPrice(new_price), pStock(new_stock)
{ }

void Product::print_info()
{
	cout << "Id\t: " << pId << endl
		<< "Name\t: " << pName << endl
		<< "Price\t: " << pPrice << endl
		<< "Stock\t: " << pStock << endl;
}

istream& operator >> (istream &in, Product &my_product)
{
	in >> my_product.pId >> my_product.pName >> my_product.pPrice >> my_product.pStock;
	return in;
}

ostream& operator << (ostream &out, Product &my_product)
{
	out << my_product.pId << endl
		<< my_product.pName << endl
		<< my_product.pPrice << endl
		<< my_product.pStock << endl;
	return out;
}
