#ifndef PRODUCT_H
#define PRODUCT_H

#include <iostream>
#include <string>
using namespace std;

class Product
{
	protected:
		int pId;
		string pName;
		double pPrice;
		int pStock;

	public:
		// Constructors
		Product();
		Product(int new_id, string new_name, double new_price, int new_stock);
		~Product() { }

		// getters
		int get_id() { return pId; }
		string get_name() { return pName; }
		double get_price() { return pPrice; }
		int get_stock() { return pStock; }

		// setters
		void set_id(int new_id) { pId = new_id; }
		void set_name(string new_name) { pName = new_name; }
		void set_price(double new_price) { pPrice = new_price; }
		void set_stock(int new_stock) { pStock = new_stock; }

		void print_info();

		// operator overloadings
		friend istream& operator >> (istream &in, Product &my_product);
		friend ostream& operator << (ostream &out, Product &my_product);
};

#endif
