#ifndef BOOK_H
#define BOOK_H

#include "Product.h"

class Book: public Product
{
	private:
		string publisher;
		string author;
		int isbn;

	public:
		// Constructors
		Book();
		Book(int new_id, string new_name, double new_price, int new_stock, string new_publisher, string new_author, int new_isbn);
		~Book() { }

		// Getters
		string get_publisher() { return publisher; }
		string get_author() { return author; }
		int get_isbn() { return isbn; }

		// Setters
		void set_publisher(string new_publisher) { publisher = new_publisher; }
		void set_author(string new_author) { author = new_author; }
		void set_isbn(int new_isbn) { isbn = new_isbn; }

		void print_info();

		friend istream& operator >> (istream &in, Book &my_book);
		friend ostream& operator << (ostream &out, Book &my_book);

};

#endif
