#include "Book.h"

Book::Book(): Product(), publisher(""), author(""), isbn(0)
{ }

Book::Book(int new_id, string new_name, double new_price, int new_stock, string new_publisher, string new_author, int new_isbn):
	Product(new_id, new_name, new_price, new_stock), publisher(new_publisher), author(new_author), isbn(new_isbn)
{ }

void Book::print_info()
{
	cout << "Book" << endl;
	Product::print_info();
	cout << "Publisher\t: " << publisher << endl
		<< "Author\t: " << author << endl
		<< "Isbn\t: " << isbn << endl;
}

istream& operator >> (istream &in, Book &my_book)
{
	in >> my_book.pId >> my_book.pName >> my_book.pPrice >> my_book.pStock >> my_book.publisher >> my_book.author >> my_book.isbn;
	// in >> static_cast<Product>(my_book) >> my_book.publisher >> my_book.author >> my_book.isbn;
	return in;
}

ostream& operator << (ostream &out, Book &my_book)
{
	out << my_book.pId << endl
		<< my_book.pName << endl
		<< my_book.pPrice << endl
		<< my_book.pStock << endl
	// out << static_cast<Product>(my_book) << endl
		<< my_book.publisher << endl
		<< my_book.author << endl
		<< my_book.isbn;
	return out;
}
