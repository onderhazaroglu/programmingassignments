#ifndef PRODUCT_H
#define PRODUCT_H

#include <iostream>
#include <string>
using namespace std;

class Product
{
	private:
		int pId;
		string pName;
		double pPrice;
		int pStock;

	public:
		// getters
		int get_id() { return pId; }
		string get_name() { return pName; }
		double get_price() { return pPrice; }
		int get_stock() { return pStock; }

		// setters
		void set_id(int new_id) { pId = new_id; }
		void set_name(string new_name) { pName = new_name; }
		void set_price(double new_price) { pPrice = new_price; }
		void set_stock(int new_stock) { pStock = new_stock; }

		void print();

		// operator overloadings
		friend istream& operator >> (istream &in, Product &my_product);
		friend ostream& operator << (ostream &out, Product &my_product);
};

#endif
