#include <iostream>
#include "Product.h"
using namespace std;

int main()
{
	Product my_product;
	cout << "Please enter the product info below:" << endl;
	cin >> my_product;

	cout << "The content of the product you entered: " << endl;
	cout << my_product;

	my_product.print();

	return 0;
}
