#include <iostream>
#include <string>
#include <fstream>
using namespace std;

int main()
{
	string filename = "cardata.txt";
	// ifstream fin( filename );
	ifstream fin( filename );

	ofstream fout_acc("acceptable.txt");
	ofstream fout_unacc("unacceptable.txt");

	string buying, maint, doors, persons, trunk, safety, fdec;
	unsigned int task2 = 0, task3 = 0;

	while( fin >> buying >> maint >> doors >> persons >> trunk >> safety >> fdec )
	{
		// Task 1
		if ( fdec == "acc" ) fout_acc << buying << " " << maint << " " << doors << " " << persons << " " << trunk << " " << safety << " " << fdec << endl;
		else
			fout_unacc << buying << " " << maint << " " << doors << " " << persons << " " << trunk << " " << safety << " " << fdec << endl;

		// Task 2
		if ( buying == "vhigh" && safety == "high" ) task2++;

		// Task 3
		if ( (buying == "med" || buying == "low") && (maint == "med" || maint == "low") && fdec == "acc" ) task3++;

		// Task 4
		cout << "Task4: " << endl;

		if ( (buying == "med" || buying == "low") &&
			(maint == "med" || maint == "low")  &&
			fdec == "acc" &&
			doors == "4" &&
			persons == "4" &&
			(trunk == "med" || trunk == "big") &&
			(safety == "med" || safety == "high") &&
			fdec == "acc" )
			cout << buying << " " << maint << " " << doors << " " << persons << " " << trunk << " " << safety << " " << fdec << endl;


	}

	cout << "Task2: " << task2 << endl;
	cout << "Task3: " << task3 << endl;


}
