import sys, os

def main():
	file='cardata.txt'
	fout_acc=open('acceptable.txt','w+')
	fout_unacc=open('unacceptable.txt','w+')
	
	task2=0
	task3=0
	
	with open(file,'r') as f:
		for line in f:
			[buying,maint,doors,persons,trunk,safety,fdec]=line.split()
			if fdec=='acc':
				fout_acc.write(line)
			else:
				fout_unacc.write(line)

			if buying=='vhigh' and safety=='high':
				task2+=1
			
			if (buying=='med' or buying=='low') and (maint=='med' or maint=='low') and fdec=='acc':
				task3+=1
				
			if (buying=='med' or buying=='low') and	\
			(maint=='med' or maint=='low') and \
			doors=='4' and \
			fdec=='acc' and \
			persons=='4' and \
			(trunk=='med' or trunk=='big') and \
			(safety=='med' or safety=='high') and \
			fdec=='acc':
				print line,
				
	fout_acc.close()
	fout_unacc.close()
	
	print 'Task 2: ',task2
	print 'Task 3: ',task3


if __name__ == "__main__":
	main()