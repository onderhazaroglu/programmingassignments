#ifndef BOOK_H
#define BOOK_H

#include "Product.h"

class Book: public Product
{
	private:
		string publisher;
		string author;
		string isbn;

	public:
		// Constructors
		Book();
		Book(string new_name, string new_price, string new_stock, string new_publisher, string new_author, string new_isbn);
		Book(const Book &src_book);
		~Book() { }

		// Getters
		string get_publisher() { return publisher; }
		string get_author() { return author; }
		string get_isbn() { return isbn; }

		// Setters
		void set_publisher(string new_publisher) { publisher = new_publisher; }
		void set_author(string new_author) { author = new_author; }
		void set_isbn(string new_isbn) { isbn = new_isbn; }

		void print_info();

		friend istream& operator >> (istream &in, Book &my_book);
		friend ostream& operator << (ostream &out, const Book &my_book);

};

#endif
