#include <iostream>
#include "Product.h"
#include "Book.h"
#include "Dvd.h"

using namespace std;

int main()
{
	Dvd my_dvd(1234, "Walking Dead", 33.99, 4, "AMC", "Andrew Lincoln", "Sarah Wayne Callies");
	my_dvd.print_info();
	Book my_book(4321, "Slaughterhouse Five", 9.99, 6, "Dial Press", "Kurt Vonnegut", 123456);
	my_book.print_info();

	return 0;
}
