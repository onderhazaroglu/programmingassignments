#include "Book.h"

Book::Book(): Product(), publisher(""), author(""), isbn("")
{ }

Book::Book(string new_name, string new_price, string new_stock, string new_publisher, string new_author, string new_isbn):
	Product(new_name, new_price, new_stock), publisher(new_publisher), author(new_author), isbn(new_isbn)
{ }

Book::Book(const Book &src_book):
	Product(src_book), publisher(src_book.publisher), author(src_book.author), isbn(src_book.isbn)
{ }

void Book::print_info()
{
	cout << "Book" << endl;
	Product::print_info();
	cout << "Publisher\t: " << publisher << endl
		<< "Author\t\t: " << author << endl
		<< "Isbn\t\t: " << isbn << endl;
}

istream& operator >> (istream &in, Book &my_book)
{
	in >> my_book.pName >> my_book.pPrice >> my_book.pStock >> my_book.publisher >> my_book.author >> my_book.isbn;
	// in >> static_cast<Product>(my_book) >> my_book.publisher >> my_book.author >> my_book.isbn;
	return in;
}

ostream& operator << (ostream &out, const Book &my_book)
{
	out << my_book.pName << endl
		<< my_book.pPrice << endl
		<< my_book.pStock << endl
	// out << static_cast<Product>(my_book) << endl
		<< my_book.publisher << endl
		<< my_book.author << endl
		<< my_book.isbn;
	return out;
}
