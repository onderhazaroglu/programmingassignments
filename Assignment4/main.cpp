#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "Product.h"
#include "Dvd.h"
#include "Book.h"
using namespace std;

// Following are the declarations of read_books() and read_dvds() functions that you need to complete.
void read_books(string book_file_name, vector<Book> &my_books);
void read_dvds(string dvd_file_name, vector<Dvd> &my_dvds);

int Product::idcount = 0;
int Product::num_products = 0;

int main()
{
	vector<Book> all_books;
	vector<Dvd> all_dvds;

	read_books("books.txt", all_books);
	read_dvds("dvds.txt", all_dvds);

	cout << "# of all instances: " << Product::get_num_products() << endl;

	
	for(unsigned int i = 0; i < all_books.size(); i++)
	{
		all_books[i].print_info();
	}

	for(unsigned int i = 0; i < all_dvds.size(); i++)
	{
		all_dvds[i].print_info();
	}

	// Product myprod1;
	// myprod1.print_info();

	// Product myprod2(myprod1);
	// myprod2.print_info();
	
	return 0;
}

// You can define read_books() and read_dvds() functions here
void read_books(string book_file_name, vector<Book> &my_books)
{
	ifstream fin(book_file_name.c_str());
	Book * temp_book = new Book;
	int k = 0;

	while( fin >>  *temp_book )
	{
		cout << "k: " << k++ << endl;
		my_books.push_back(*temp_book);
		delete temp_book;

		if (!fin.eof() )
		{
			temp_book = new Book;
			cout << "there is more books" << endl;
		}
	}
}


void read_dvds(string dvd_file_name, vector<Dvd> &my_dvds)
{
	ifstream fin(dvd_file_name.c_str());
	Dvd * temp_dvd = new Dvd;
	int k = 0;

	while( fin >> *temp_dvd )
	{
		cout << "k: " << k++ << endl;
		my_dvds.push_back(*temp_dvd);
		delete temp_dvd;

		if (!fin.eof())
		{
			temp_dvd = new Dvd;
			cout << "there is more dvds" << endl;
		}
	}
}
