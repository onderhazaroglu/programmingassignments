#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include "Product.h"
#include "Dvd.h"
#include "Book.h"
using namespace std;

// Following are the declarations of read_books() and read_dvds() functions that you need to complete.
void read_books(string book_file_name, vector<Book> &my_books);
void read_dvds(string dvd_file_name, vector<Dvd> &my_dvds);

// book and dvd add functions
void add_book(vector<Book> &my_books);
void add_dvd(vector<Dvd> &my_dvds);

void write_books(string book_file_name, vector<Book> &my_books);
void write_dvds(string dvd_file_name, vector<Dvd> &my_dvds);

// Template function prototype
template <class T>
void print_product_vector(vector<T> my_products);


int Product::idcount = 0;
int Product::num_products = 0;

int main()
{
	vector<Book> all_books;
	vector<Dvd> all_dvds;

	read_books("books.txt", all_books);
	read_dvds("dvds.txt", all_dvds);

	// cout << "# of all instances: " << Product::get_num_products() << endl;

	int choice;
	while( true )
	{
		cout << "Enter a number to manage inventory" << endl
			<< "1.) See all books in stock" << endl
			<< "2.) See all dvds in stock" << endl
			<< "3.) Add a book to stock" << endl
			<< "4.) Add a dvd to stock" << endl
			<< "0.) Quit program" << endl
			<< ":";
		cin >> choice;

		if ( choice == 1 )
		{
			print_product_vector(all_books);
		}
		else if ( choice == 2)
		{
			print_product_vector(all_dvds);
		}
		else if ( choice == 3)
		{
			add_book(all_books);
		}
		else if ( choice == 4)
		{
			add_dvd(all_dvds);
		}
		else
		{
			break;
		}

	}

	write_books("books.txt", all_books);
	write_dvds("dvds.txt", all_dvds);

	return 0;
}

// You can define read_books() and read_dvds() functions here
void read_books(string book_file_name, vector<Book> &my_books)
{
	ifstream fin(book_file_name.c_str());
	Book * temp_book = new Book;
	// int k = 0;

	while( fin >>  *temp_book )
	{
		// cout << "k: " << k++ << endl;
		my_books.push_back(*temp_book);
		delete temp_book;

		if (!fin.eof() )
		{
			temp_book = new Book;
			// cout << "there is more books" << endl;
		}
	}
	fin.close();
}


void read_dvds(string dvd_file_name, vector<Dvd> &my_dvds)
{
	ifstream fin(dvd_file_name.c_str());
	Dvd * temp_dvd = new Dvd;
	// int k = 0;

	while( fin >> *temp_dvd )
	{
		// cout << "k: " << k++ << endl;
		my_dvds.push_back(*temp_dvd);
		delete temp_dvd;

		if (!fin.eof())
		{
			temp_dvd = new Dvd;
			// cout << "there is more dvds" << endl;
		}
	}
	fin.close();
}

void add_book(vector<Book> &my_books)
{
	Book new_book;
	string new_name, new_price, new_stock, new_publisher, new_author, new_isbn;
	cout << "Enter the new book information:" << endl;
	cout << "Book name: "; cin >> new_name; new_book.set_name(new_name);
	cout << "Price: "; cin >> new_price; new_book.set_price(new_price);
	cout << "Number in stock: "; cin >> new_stock; new_book.set_stock(new_stock);
	cout << "Publisher: "; cin >> new_publisher; new_book.set_publisher(new_publisher);
	cout << "Author: "; cin >> new_author; new_book.set_author(new_author);
	cout << "Isbn: "; cin >> new_isbn; new_book.set_isbn(new_isbn);

	my_books.push_back(new_book);
}

void add_dvd(vector<Dvd> &my_dvds)
{
	Dvd new_dvd;
	string new_name, new_price, new_stock, new_studio, new_male_lead, new_female_lead;
	cout << "Enter the new dvd information:" << endl;
	cout << "Dvd name: "; cin >> new_name; new_dvd.set_name(new_name);
	cout << "Price: "; cin >> new_price; new_dvd.set_price(new_price);
	cout << "Number in stock: "; cin >> new_stock; new_dvd.set_stock(new_stock);
	cout << "Studio: "; cin >> new_studio; new_dvd.set_movie_studio(new_studio);
	cout << "Male lead: "; cin >> new_male_lead; new_dvd.set_male_lead(new_male_lead);
	cout << "Female lead: "; cin >> new_female_lead; new_dvd.set_female_lead(new_female_lead);

	my_dvds.push_back(new_dvd);
}

void write_books(string book_file_name, vector<Book> &my_books)
{
	ofstream fout(book_file_name.c_str());
	for(unsigned int i=0; i< my_books.size(); i++)
	{
		fout << my_books[i];
		if ( i < my_books.size()-1 ) fout << endl;
	}
	fout.close();
}

void write_dvds(string dvd_file_name, vector<Dvd> &my_dvds)
{
	ofstream fout(dvd_file_name.c_str());
	for(unsigned int i=0; i< my_dvds.size(); i++)
	{
		fout << my_dvds[i];
		if ( i < my_dvds.size()-1 ) fout << endl;
	}
	fout.close();
}

template <class T>
void print_product_vector(vector<T> my_products)
{
	for(unsigned int i = 0; i < my_products.size(); i++)
	{
		my_products[i].print_info();
	}
	cout << endl;
}

