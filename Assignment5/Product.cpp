#include "Product.h"

Product::Product(): pId(idcount++), pName(""), pPrice(""), pStock("")
{
	num_products++;
}

Product::Product(string new_name, string new_price, string new_stock):
	pId(idcount++), pName(new_name), pPrice(new_price), pStock(new_stock)
{
	num_products++;
}

Product::Product(const Product &src_product):
	pId(src_product.pId), pName(src_product.pName), pPrice(src_product.pPrice), pStock(src_product.pStock)
{
	// cout << "Product copy constructor is invoked" << endl;
	num_products++;
}

Product::~Product()
{
	num_products--;
}

void Product::print_info()
{
	cout << "Id\t\t: " << pId << endl
		<< "Name\t\t: " << pName << endl
		<< "Price\t\t: " << pPrice << endl
		<< "Stock\t\t: " << pStock << endl;
}

istream& operator >> (istream &in, Product &my_product)
{
	in >> my_product.pName >> my_product.pPrice >> my_product.pStock;
	return in;
}

ostream& operator << (ostream &out, const Product &my_product)
{
	out	<< my_product.pName << endl
		<< my_product.pPrice << endl
		<< my_product.pStock << endl;
	return out;
}
