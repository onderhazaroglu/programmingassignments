#ifndef PRODUCT_H
#define PRODUCT_H

#include <iostream>
#include <string>
using namespace std;

class Product
{
	protected:
		int pId;
		string pName;
		string pPrice;
		string pStock;

	private:
		static int num_products;
		static int idcount;

	public:
		// Constructors
		Product();
		Product(string new_name, string new_price, string new_stock);
		Product(const Product &src_product);
		~Product();

		// getters
		int get_id() { return pId; }
		string get_name() { return pName; }
		string get_price() { return pPrice; }
		string get_stock() { return pStock; }
		static int get_num_products() { return num_products; }
		static int get_idcount() { return idcount; }

		// setters
		// void set_id(int new_id) { pId = new_id; }
		void set_name(string new_name) { pName = new_name; }
		void set_price(string new_price) { pPrice = new_price; }
		void set_stock(string new_stock) { pStock = new_stock; }

		void print_info();

		// operator overloadings
		friend istream& operator >> (istream &in, Product &my_product);
		friend ostream& operator << (ostream &out, const Product &my_product);
};

#endif
