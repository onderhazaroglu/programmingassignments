#ifndef DVD_H
#define DVD_H

#include "Product.h"

class Dvd: public Product
{
	private:
		string movie_studio;
		string male_lead;
		string female_lead;

	public:
		// Constructors
		Dvd();
		Dvd(string new_name, string new_price, string new_stock, string new_movie_studio, string new_male_lead, string new_female_lead);
		Dvd(const Dvd &src_dvd);
		~Dvd() { }

		// Getters
		string get_movie_studio() { return movie_studio; }
		string get_male_lead() { return male_lead; }
		string get_female_lead() { return female_lead; }

		// Setters
		void set_movie_studio(string new_movie_studio) { movie_studio = new_movie_studio; }
		void set_male_lead(string new_male_lead) { male_lead = new_male_lead; }
		void set_female_lead(string new_female_lead) { female_lead = new_female_lead; }

		void print_info();

		friend istream& operator >> (istream &in, Dvd &my_dvd);
		friend ostream& operator << (ostream &out, const Dvd &my_dvd);

};

#endif
