#include <iostream>
#include <string>
#include "BankAccount.h"
#include "CheckingAccount.h"

using namespace std;

bool equal_balance_non_friend(const BankAccount &acc1, const BankAccount &acc2);
bool equal_balance_friend(const BankAccount &acc1, const BankAccount &acc2);

int main()
{
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);

	BankAccount account1("Onder", "Hazar", 100, 99, 2.5), account2("Gokce", "Hazar", 70, 55, 3.5);

	// account1.set();

	// account1.print_values();
	cout << account1 << endl;
	account2.print_values();

	{
		BankAccount account3(account2);
		account3.print_values();
	}

	if ( equal_balance_non_friend(account1, account2) ) cout << "balances are equal (non-friend)\n";
	else cout << "balances are not equal - nf.\n";

	if ( account1.equal_balance(account2) ) cout << "balances are equal (member)\n";
	else cout << "balances are not equal - mem.\n";

	if ( equal_balance_friend(account1, account2) ) cout << "balances are equal (friend) \n";
	else cout << "balances are not equal - fr.\n";


	BankAccount account4;
	account4 = account1 + account2;
	account4.print_values();


	BankAccount account5;
	account5 = -account4;
	account5.print_values();

	BankAccount account6;
	// account6 = ++account4;
	account6 = account4++;
	account6.print_values();

	BankAccount account7;
	account7 = account4;
	account7.print_values();
	

	CheckingAccount checking1;
//	cout << checking1 << endl;



	int a;
	cin >> a;

	return 0;
}

bool equal_balance_non_friend(const BankAccount &acc1, const BankAccount &acc2)
{
	if ( acc1.get_dollars() == acc2.get_dollars() && acc1.get_cents() == acc2.get_cents() ) return true;
	else return false;
}

bool equal_balance_friend(const BankAccount &acc1, const BankAccount &acc2)
{
	if ( acc1.dollars == acc2.dollars && acc1.cents == acc2.cents ) return true;
	else return false;
}

BankAccount operator+ (const BankAccount &acc1, const BankAccount &acc2)
{
	BankAccount temp;
	temp.dollars = acc1.dollars + acc2.dollars;
	temp.cents = acc1.cents + acc2.cents;
	if ( temp.cents >= 100 )
	{
		temp.dollars++;
		temp.cents %= 100;
	}
	return temp;
}