#include <iostream>
#include "BankAccount.h"
using namespace std;

BankAccount::BankAccount()
{
	fname = lname = "";
	dollars = cents = 0;
	interest_rate = 0.0;
}

BankAccount::BankAccount(string new_fname, string new_lname, int new_dollars, int new_cents, double new_rate):
fname(new_fname), lname(new_lname), dollars(new_dollars), cents(new_cents), interest_rate(new_rate)
{
	/*
	fname = new_fname;
	lname = new_lname;
	dollars = new_dollars;
	cents = new_cents;
	interest_rate = new_rate;
	*/
}

BankAccount::BankAccount(const BankAccount &src_acc)
{
	fname = src_acc.fname;
	lname = src_acc.lname;
	dollars = src_acc.dollars;
	cents = src_acc.cents;
	interest_rate = src_acc.interest_rate;
	cout << "BankAccount copy constructor is called.\n";
}

BankAccount::~BankAccount()
{
	// fill this part if you add dynamic variables
	cout << "BankAccount destructor is called.\n";
}

void BankAccount::set()
{
	cout << "Please enter first name: ";
	cin >> fname;
	cout << "Please enter last name: ";
	cin >> lname;
	cout << "Please enter dollars: ";
	cin >> dollars;
	cout << "Please enter cents: ";
	cin >> cents;
	cout << "Please enter interest rate: ";
	cin >> interest_rate;
	cout << "Account information is set successfully.\n";
}

void BankAccount::print_values()
{
	cout << "Account owner\t: " << fname << " " << lname << endl
		<< "Balance\t\t: " << dollars + cents*0.01 << endl
		<< "Interest rate\t: " << interest_rate << endl;
}

bool BankAccount::equal_balance(const BankAccount &acc1)
{
	if ( this->dollars == acc1.dollars && this->cents == acc1.cents ) return true;
	else return false;
}

BankAccount& BankAccount::operator-()
{
	this->dollars = -this->dollars;
	this->cents = -this->cents;
	return *this;
}

BankAccount& BankAccount::operator++()
{
	++this->dollars;
	return *this;
}

BankAccount& BankAccount::operator++(int)
{
	this->dollars++;
	return *this;
}

BankAccount& BankAccount::operator=(const BankAccount & src_acc)
{
	this->fname = src_acc.fname;
	this->lname = src_acc.lname;
	this->dollars = src_acc.dollars;
	this->cents = src_acc.cents;
	return *this;
}

ostream& operator<< (ostream &my_outs, const BankAccount &acc)
{
	my_outs << "Account owner\t: " << acc.fname << " " << acc.lname << endl
		<< "Balance\t\t: " << acc.dollars + acc.cents*0.01 << endl
		<< "Interest rate\t: " << acc.interest_rate << endl;
	return my_outs;
}

istream& operator>> (istream &my_ins, BankAccount &acc)
{
	my_ins >> acc.fname >> acc.lname >> acc.dollars >> acc.cents >> acc.interest_rate;
	return my_ins;
}