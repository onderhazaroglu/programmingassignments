#ifndef BANKACCOUNT_H
#define BANKACCOUNT_H

#include <string>
using namespace std;

class BankAccount
{
protected:
	string fname;
	string lname;
	int dollars;
	int cents;
	double interest_rate;

public:
	// Constructors
	BankAccount();	// default constructor
	BankAccount(string new_fname, string new_lname, int new_dollars, int new_cents, double new_rate);

	// Copy constructor
	BankAccount(const BankAccount &src_acc);

	// Destructor
	~BankAccount();

	// Accessor
	string get_fname() const { return fname; }
	string get_lname() const { return lname; }
	int get_dollars() const { return dollars; }
	int get_cents() const { return cents; }
	double get_rate() const { return interest_rate; }

	// Mutator
	void set_fname(string new_fname) { fname = new_fname; }
	void set_lname(string new_lname) { lname = new_lname; }
	void set_dollars(int new_dollars) { dollars = new_dollars; }
	void set_cents(int new_cents) { cents = new_cents; }
	void set_rate(double new_rate) { interest_rate = new_rate; }

	void set();
	void print_values();

	bool equal_balance(const BankAccount &acc1);

	friend bool equal_balance_friend(const BankAccount &acc1, const BankAccount &acc2);

	friend BankAccount operator+ (const BankAccount &acc1, const BankAccount &acc2);

	BankAccount& operator-();

	BankAccount& operator++();		// prefix
	BankAccount& operator++(int);	// postfix

	BankAccount& operator=(const BankAccount & src_acc);

	friend ostream& operator<< (ostream &my_outs, const BankAccount &acc);
	friend istream& operator>> (istream &my_ins, BankAccount &acc);
};

#endif