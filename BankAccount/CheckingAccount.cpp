#include <iostream>
#include "CheckingAccount.h"
using namespace std;

CheckingAccount::CheckingAccount(): BankAccount(), num_checks(0)
{ }

CheckingAccount::CheckingAccount(string new_fname, string new_lname, int new_dollars, int new_cents, double new_rate, int new_num_checks)
{
	BankAccount(new_fname, new_lname, new_dollars, new_cents, new_rate);
	num_checks = new_num_checks;
}

CheckingAccount::CheckingAccount(const CheckingAccount &src_checking): BankAccount(src_checking)
{
	// BankAccount(src_checking);		// should be in the initialization section
	this->num_checks = src_checking.num_checks;
}

CheckingAccount::~CheckingAccount()
{
	cout << "CheckingAccount destructor is invoked.\n";
	// fill this part if add dynamic variable
}

void CheckingAccount::deposit(double amount)
{
	dollars += int(amount);
	cents += int( (amount - int(amount))*100 );
	if ( cents > 100 )
	{
		dollars++;
		cents %= 100;
	}
}

void CheckingAccount::withdraw(double amount)
{
	dollars -= int(amount);
	cents -= int( (amount - int(amount))*100 );
}

void CheckingAccount::print_values()
{
	BankAccount::print_values();
	cout << "# of checks\t: " << num_checks << endl << endl;
}