#pragma once

#include <iostream>
#include <string>
#include "BankAccount.h"
using namespace std;

class CheckingAccount: public BankAccount
{
protected:
	int num_checks;

public:
	// Constructors
	CheckingAccount();
	CheckingAccount(string new_fname, string new_lname, int new_dollars, int new_cents, double new_rate, int new_num_checks);

	// Copy constructor
	CheckingAccount(const CheckingAccount &src_checking);

	// Destructor
	~CheckingAccount();

	// Getters
	int get_num_checks() { return num_checks; }

	// Setters
	void set_num_checks(int new_num_checks) { num_checks = new_num_checks; }

	void deposit(double amount);
	void withdraw(double amount);

	void print_values();

};