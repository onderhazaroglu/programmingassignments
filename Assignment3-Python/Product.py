# import sys, os
class Product(object):

	def __init__(self,*args, **kwargs):
	
		#print 'args: ',len(args)
		#print 'kwargs: ',len(kwargs)
		
		attr=['pId','pName','pPrice','pStock']
		itr=iter(attr)
		
		for i in args:
			setattr(self,next(itr),i)
			
		for i in kwargs.keys():
			if i in attr:
				setattr(self,i,kwargs[i])
		
		if not hasattr(self,'pId'):
			self.pId=0
		if not hasattr(self,'pName'):
			self.pName=''
		if not hasattr(self,'pPrice'):
			self.pPrice=0
		if not hasattr(self,'pStock'):
			self.pStock=0

	def __str__(self):
		return 'Product Id: '+str(self.pId)+'\nName: '+self.pName+'\nPrice: '+str(self.pPrice)+'\nStock: '+str(self.pStock)

	def print_info(self):
		print 'Product Id\t: '+str(self.pId)+'\nName\t\t: '+self.pName+'\nPrice\t\t: '+str(self.pPrice)+'\nStock\t\t: '+str(self.pStock)

	# getter functions
	def get_id(self):
		return self.pId

	def get_name(self):
		return self.name
	
	def get_price(self):
		return self.pPrice
		
	def get_stock(self):
		return self.pStock
		
	# setter functions
	def set_id(self,newId):
		self.pId=newId
	
	def set_name(self,newName):
		self.pName=newName
	
	def set_price(self,newPrice):
		self.pPrice=newPrice
		
	def set_stock(self,newStock):
		self.pStock=newStock