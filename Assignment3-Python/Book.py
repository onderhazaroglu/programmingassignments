from Product import *

class Book(Product):
	
	def __init__(self,*args, **kwargs):
		pargs=args[0:4]
		bargs=args[4:7]
		
		super(Book, self).__init__(*pargs, **kwargs)
		
		battr=['publisher','author','isbn']
		bitr=iter(battr)
		for i in bargs:
			setattr(self, next(bitr), i)
		
		for i in kwargs.keys():
			if i in battr:
				setattr(self, i, kwargs[i])
	
		if not hasattr(self,'publisher'):
			self.publisher=''
		if not hasattr(self,'author'):
			self.author=''
		if not hasattr(self,'isbn'):
			self.isbn=0
		
	def __str__(self):
		return super(Book, self).__str__()+'Publisher: '+str(self.publisher)+'\nAuthor: '+self.author+'\nIsbn: '+str(self.isbn)
		
	def print_info(self):
		print self.__class__.__name__
		super(Book, self).print_info()
		print 'Publisher\t: '+str(self.publisher)+'\nAuthor\t\t: '+self.author+'\nIsbn\t\t: '+str(self.isbn)+'\n'