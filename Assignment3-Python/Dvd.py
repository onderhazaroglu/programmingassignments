from Product import *

class Dvd(Product):
	
	def __init__(self,*args, **kwargs):
		pargs=args[0:4]
		dargs=args[4:7]
		
		super(Dvd, self).__init__(*pargs, **kwargs)
		
		dattr=['mstudio','mlead','flead']
		ditr=iter(dattr)
		for i in dargs:
			setattr(self, next(ditr), i)
		
		for i in kwargs.keys():
			if i in dattr:
				setattr(self, i, kwargs[i])
	
		if not hasattr(self,'mstudio'):
			self.mstudio=''
		if not hasattr(self,'mlead'):
			self.mlead=''
		if not hasattr(self,'flead'):
			self.flead=0
		
	def __str__(self):
		return super(Dvd, self).__str__()+'\nMovie Studio: '+self.mstudio+'\nMale Lead: '+self.mlead+'\nFemale Lead: '+self.flead
		
	def print_info(self):
		print self.__class__.__name__
		super(Dvd, self).print_info()
		print 'Movie Studio\t: '+self.mstudio+'\nMale Lead\t: '+self.mlead+'\nFemale Lead\t: '+self.flead+'\n'