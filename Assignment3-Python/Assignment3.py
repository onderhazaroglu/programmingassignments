import sys, os
from Product import *
from Book import *
from Dvd import *

def main():
#	prod1=Product()
#	print prod1.__dict__
#	print prod1.pId
	
#	prod2=Product(5,"first product",10.99,50, pPrice=15.99)
#	print prod2.__dict__
#	print prod2.pStock
#	print prod2.get_stock()

#	prod3=Product(9)
#	print prod3
#	print prod3.__dict__
	
	my_dvd=Dvd(1234, "Walking Dead", 33.99, 4, "AMC", "Andrew Lincoln", "Sarah Wayne Callies")
	my_dvd.print_info()
	my_book=Book(4321, "Slaughterhouse Five", 9.99, 6, "Dial Press", "Kurt Vonnegut", 123456)
	my_book.print_info()
	
	print my_dvd
	print my_book

if __name__ == "__main__":
	main()