import sys, os
from Product import *

def main():
	prod1=Product()
	print prod1.__dict__
	print prod1.pId
	
	prod2=Product(5,"first product",10.99,50, pPrice=15.99)
	print prod2.__dict__
	print prod2.pStock
	print prod2.get_stock()

	prod3=Product(9)
	print prod3
	print prod3.__dict__

	
'''
	with open(file,'r') as f:
		for line in f:
			[buying,maint,doors,persons,trunk,safety,fdec]=line.split()
			if fdec=='acc':
				fout_acc.write(line)
'''

if __name__ == "__main__":
	main()